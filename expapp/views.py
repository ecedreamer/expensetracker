from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .models import Expense, Student
from django.db.models.functions import TruncMonth, TruncWeek, TruncDay
from django.db.models import Count, Sum

class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        expenses = Expense.objects.annotate(
            day=TruncDay('date')).values('day').annotate(
                total=Sum('amount')).values('day', 'total')
        context['expenses'] = expenses


        return context


 
class ExpenseListView(TemplateView):
    template_name = "myexpense.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        my_exps = Expense.objects.all().order_by("-date")
        context["all_expenses"] = my_exps

        total = 0
        for expense in my_exps:
            total += expense.amount

        context['total_expense'] = total

        
        return context


class NepalView(TemplateView):
    template_name = "nepal.html"


class ExpenseEntryView(TemplateView):
    template_name = "addexpense.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["all_students"] = Student.objects.all()
        return context

    def post(self, request):
        # t = request.POST["topic"]
        t = request.POST.get("topic")
        a = request.POST.get("amount")
        d = request.POST.get("date")
        s_id = request.POST.get("exp_by")
        s = Student.objects.get(id=s_id)
        Expense.objects.create(topic=t, amount=a, date=d, student=s)
        return redirect("/expenses/")
