from django.contrib import admin
from .models import Expense, Student

admin.site.register(Student)
admin.site.register(Expense)

