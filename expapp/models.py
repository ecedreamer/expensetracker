from django.db import models


class Student(models.Model):
    name = models.CharField(max_length=100)
    college = models.CharField(max_length=200, null=True, blank=True)
    mobile = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name


class Expense(models.Model):
    topic = models.CharField(max_length=200)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, null=True, blank=True)
    amount = models.IntegerField()
    date = models.DateField()

    def __str__(self):
        return self.topic
